var Person = function (obj) {
	if (obj) {
		for (let key in obj) {
			// 确定传入对象的key属性也是Person.prototype的属性
			if (Object.prototype.hasOwnProperty.call(Person.prototype, key)) {
				this[key] = obj[key];
			}
		}
		if (this.birthday && this.birthday != -1) {
			this.age = new Date().getYear() - new Date(this.birthday).getYear();
		}
	}
}

Person.prototype = {
	id: -1, // -1表示还未获取
	name: '',
	img: '',
	c_name: '',
	f_name: '',
	sex: -1, // 1男，0女，默认1
	father: -1, // id
	mother: -1, // id
	spouse: -1, // id
	birthday: -1,
	deathday: -1,
	phone: -1,
	weixin: '',
	address: '',
	native_place: '',
	ancestors: '',
	nation: '',
}

module.exports = Person;