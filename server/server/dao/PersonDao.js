var dbutil = require("./DBUtil");

function insertPerson(name, img, c_name, f_name, sex, father, mother, spouse, birthday, deathday, phone, weixin, address, native_place, ancestors, nation, success, fail) {
    var insertSql = "insert into person (`name`, `img`, `c_name`, `f_name`, `sex`, `father`, `mother`, `spouse`, `birthday`, `deathday`, `phone`, `weixin`, `address`, `native_place`, `ancestors`, `nation`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? , ? ,?, ?);";
    var params = [name, img, c_name, f_name, sex, father, mother, spouse, birthday, deathday, phone, weixin, address, native_place, ancestors, nation];
    var connection = dbutil.createConnection();
    connection.connect();
    try {
        connection.query(insertSql, params, function (error, result) {
            if (error == null) {
                success(result);
            } else {
                fail(error);
            }
        });
    } catch (error) {
        fail(error);
    }
    connection.end();
}

function queryAllPerson(success, fail) {
    var insertSql = "select * from person;";
    var connection = dbutil.createConnection();
    connection.connect();
    try {
        connection.query(insertSql, null, function (error, result) {
            if (error == null) {
                success(result);
            } else {
                fail(error);
            }
        });
    } catch (error) {
        fail('数据库异常，获取失败');
    }
    connection.end();
}

function queryPersonByPage(pageNo, pageSize, success, fail) {
    var insertSql = "select * from person order by id asc limit ?, ?;";
    var connection = dbutil.createConnection();
    var params = [pageNo * pageSize, pageSize];
    connection.connect();
    try {
        connection.query(insertSql, params, function (error, result) {
            if (error == null) {
                success(result);
            } else {
                console.log(error);
            }
        });
    } catch (error) {
        fail('数据库异常，获取失败');
    }
    connection.end();
}

function queryPersonByLike(like, success, fail) {
    var insertSql = "select * from person where name like concat(concat('%', ?), '%') or c_name like concat(concat('%', ?), '%') or f_name like concat(concat('%', ?), '%');";
    var connection = dbutil.createConnection();
    var params = [like, like, like];
    connection.connect();
    try {
        connection.query(insertSql, params, function (error, result) {
            if (error == null) {
                success(result);
            } else {
                console.log(error);
            }
        });
    } catch (error) {
        fail('数据库异常，获取失败');
    }
    connection.end();
}

function queryPersonById(id, success, fail) {
    var insertSql = "select * from person where id=?;";
    var connection = dbutil.createConnection();
    var params = [id];
    connection.connect();
    try {
        connection.query(insertSql, params, function (error, result) {
            if (error == null) {
                success(result);
            } else {
                console.log(error);
            }
        });
    } catch (error) {
        fail('数据库异常，获取失败');
    }
    connection.end();
}

function queryBrotherAndSister(father, mother, single_parents, success, fail) {
    var insertSql = "select * from person";
    if (single_parents) {
        insertSql += " where father=? or mother=?";
    } else {
        insertSql += " where father=? and mother=?";
    }
    var connection = dbutil.createConnection();
    var params = [father, mother];
    connection.connect();
    try {
        connection.query(insertSql, params, function (error, result) {
            if (error == null) {
                success(result);
            } else {
                console.log(error);
            }
        });
    } catch (error) {
        fail('数据库异常，获取失败');
    }
    connection.end();
}

module.exports.insertPerson = insertPerson;
module.exports.queryAllPerson = queryAllPerson;
module.exports.queryPersonByPage = queryPersonByPage;
module.exports.queryPersonById = queryPersonById;
module.exports.queryBrotherAndSister = queryBrotherAndSister;
module.exports.queryPersonByLike = queryPersonByLike;