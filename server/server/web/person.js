var express = require('express');
var router = express.Router();
var url = require("url");
var personDao = require('../dao/PersonDao');
var response = null;

router.post('/insertPerson', function (req, res, next) {
    response = res;
    console.log('insertPerson : ', req.body);
    var data = null;
    req.on('data', function (data) {
        console.log('data : ', req.body, data);
        data += !!data ? data : "";
    });
    req.on('end', function () {
        console.log('data end : ', req.body, data);
    });
    if (!data) {
        data = req.body;
    }
    personDao.insertPerson(data.name, data.img, data.c_name, data.f_name, data.sex, data.father, data.mother, data.spouse, data.birthday, data.deathday, data.phone, data.weixin, data.address, data.native_place, data.ancestors, data.nation, success, fail);
});

router.get('/queryAllPerson', function (req, res, next) {
    response = res;
    personDao.queryAllPerson(success, fail);
});

router.get('/queryPersonByPage', function (req, res, next) {
    response = res;
    var params = url.parse(req.url, true).query;
    var pageNo = params.pageNo ? params.pageNo : 0;
    var pageSize = params.pageSize ? params.pageSize : 10;
    personDao.queryPersonByPage(parseInt(pageNo), parseInt(pageSize), success, fail);
});

router.get('/queryPersonById', function (req, res, next) {
    response = res;
    var params = url.parse(req.url, true).query;
    if (params.id) {
        personDao.queryPersonById(params.id, success, fail);
    } else {
        fail('id is null!');
    }
});

router.get('/queryBrotherAndSister', function (req, res, next) {
    response = res;
    var params = url.parse(req.url, true).query;
    var pageNo = params.pageNo ? params.pageNo : 0;
    var pageSize = params.pageSize ? params.pageSize : 10;
    personDao.queryBrotherAndSister(father, mother, single_parents, success, fail);
});

router.get('/queryPersonByLike', function (req, res, next) {
    response = res;
    personDao.queryPersonByLike(like, success, fail);
});

function success(data) {
    console.log(data);
    response.writeHead(200);
    response.write(writeResult("success", "查询成功", data));
    response.end();
}

function fail(err) {
    console.log(err);
    response.writeHead(400);
    response.write(writeResult("fail", "请求失败", null));
    response.end();
}

function writeResult(status, msg, data) {
    return JSON.stringify({
        status: status,
        msg: msg,
        data: data
    });
}

module.exports = router;