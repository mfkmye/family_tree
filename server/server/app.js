var express = require('express');
var url = require("url");
var cookieParser = require('cookie-parser');
var personRouter = require('./web/person');
var bodyParser = require("body-parser");

var app = express();
app.use(express.static("./dist/"));
app.use(cookieParser());
app.use(bodyParser.json());
//设置允许跨域访问该服务.
app.use('*', function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header("Access-Control-Allow-Headers", "Content-Type,Content-Length, Authorization, Accept,X-Requested-With");
  res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
  res.header('Content-Type', 'application/json;charset=utf-8');
  next();
});

app.all("/*", function (req, res, next) {
  var params = url.parse(req.url, true).query;
  console.log(req.url);
  params || console.log("params : ", JSON.stringify(params));
  next();
});

app.get('/', function (req, res, next) {
  res.writeHead(200);
  res.write('hellow');
  res.end();
})
app.use('/api', personRouter);

app.listen(80, function () {
  console.log("服务器已启动");
});